// Copyright (c) 2022 Workchat. All Rights Reserved..
// See LICENSE.txt for license information.

enum Groups {
    SYNCABLE_TYPE_TEAM = 'team',
    SYNCABLE_TYPE_CHANNEL = 'channel',
}

export default Groups;
