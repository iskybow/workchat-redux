// Copyright (c) 2022 Workchat. All Rights Reserved..
// See LICENSE.txt for license information.
export const ScopeTypes = {
    TEAM: 'team',
    CHANNEL: 'channel',
};
