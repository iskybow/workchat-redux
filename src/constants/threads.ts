// Copyright (c) 2022 Workchat. All Rights Reserved..
// See LICENSE.txt for license information.

export default {
    THREADS_CHUNK_SIZE: 20,
};
