// Copyright (c) 2022 Workchat. All Rights Reserved..
// See LICENSE.txt for license information.

import {ChannelCategoryType} from 'types/channel_categories';

export const CategoryTypes: {[name: string]: ChannelCategoryType} = {
    FAVORITES: 'favorites',
    CHANNELS: 'channels',
    DIRECT_MESSAGES: 'direct_messages',
    CUSTOM: 'custom',
};
