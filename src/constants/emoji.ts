// Copyright (c) 2022 Workchat. All Rights Reserved..
// See LICENSE.txt for license information.
export default {
    SORT_BY_NAME: 'name',
};
