// Copyright (c) 2022 Workchat. All Rights Reserved..
// See LICENSE.txt for license information.

export default {
    MEMBERS: 'members',
    GUESTS: 'guests',
    ADMINS: 'admins',
};
