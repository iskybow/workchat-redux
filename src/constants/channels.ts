// Copyright (c) 2022 Workchat. All Rights Reserved..
// See LICENSE.txt for license information.

export const NotificationLevel = {
    DEFAULT: 'default',
    ALL: 'all',
    MENTION: 'mention',
    NONE: 'none',
};

export const MarkUnread = {
    ALL: 'all',
    MENTION: 'mention',
};
