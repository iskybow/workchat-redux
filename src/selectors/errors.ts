// Copyright (c) 2022 Workchat. All Rights Reserved..
// See LICENSE.txt for license information.

import {GlobalState} from 'types/store';

export function getDisplayableErrors(state: GlobalState) {
    return state.errors.filter((error) => error.displayable);
}
