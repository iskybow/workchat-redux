// Copyright (c) 2022 Workchat. All Rights Reserved..
// See LICENSE.txt for license information.
import keyMirror from 'utils/key_mirror';

export default keyMirror({
    RECEIVED_PREFERENCES: null,
    RECEIVED_ALL_PREFERENCES: null,
    DELETED_PREFERENCES: null,
});
