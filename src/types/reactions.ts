// Copyright (c) 2022 Workchat. All Rights Reserved..
// See LICENSE.txt for license information.
export type Reaction = {
    user_id: string;
    post_id: string;
    emoji_name: string;
    create_at: number;
};
