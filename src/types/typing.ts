// Copyright (c) 2022 Workchat. All Rights Reserved..
// See LICENSE.txt for license information.
export type Typing = {
    [x: string]: {
        [x: string]: number;
    };
};
