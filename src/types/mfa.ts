// Copyright (c) 2022 Workchat. All Rights Reserved..
// See LICENSE.txt for license information.

export type MfaSecret = {
    secret: string;
    qr_code: string;
};
