// Copyright (c) 2022 Workchat. All Rights Reserved..
// See LICENSE.txt for license information.

export type TermsOfService = {
    id: string;
    create_at: number;
    user_id: string;
    text: string;
}
