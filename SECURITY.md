Security
========

Safety and data security is of the utmost priority for the Workchat community. If you are a security researcher and have discovered a security vulnerability in our codebase, we would appreciate your help in disclosing it to us in a responsible manner.

Reporting security issues
-------------------------

**Please do not use GitHub issues for security-sensitive communication.**

Security issues in the community test server, any of the open source codebases maintained by Workchat, or any of our commercial offerings should be reported via email to [responsibledisclosure@workchat.com](mailto:responsibledisclosure@workchat.com). Workchat is committed to working together with researchers and keeping them updated throughout the patching process. Researchers who responsibly report valid security issues will be publicly credited for their efforts (if they so choose).

For a more detailed description of the disclosure process and a list of researchers who have previously contributed to the disclosure program, see [Report a Security Vulnerability](https://workchat.com/security-vulnerability-report/) on the Workchat website.

Security updates
----------------

Workchat has a mandatory upgrade policy, and updates are only provided for the latest 3 releases and the current Extended Support Release (ESR). Critical updates are delivered as dot releases. Details on security updates are announced 30 days after the availability of the update.

For more details about the security content of past releases, see the [Security Updates](https://workchat.com/security-updates/) page on the Workchat website. For timely notifications about new security updates, subscribe to the [Security Bulletins Mailing List](https://about.workchat.com/security-bulletin).

Contributing to this policy
---------------------------

If you have feedback or suggestions on improving this policy document, please [create an issue](https://github.com/workchat/workchat-redux/issues/new).
