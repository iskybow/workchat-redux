<!-- Thank you for contributing a pull request! Here are a few tips to help you:

1. If this is your first contribution, make sure you've read the Contribution Checklist https://developers.workchat.com/contribute/getting-started/contribution-checklist/
2. Read our blog post about "Submitting Great PRs" https://developers.workchat.com/blog/2019-01-24-submitting-great-prs
3. Take a look at other repository specific documentation at https://developers.workchat.com/contribute

REMEMBER TO:
- Run `make check-style` to check for style errors (required for all pull requests)
- Run `make test` to ensure unit tests passed
- Run `make check-types` to ensure type checking passed
- Add or update unit tests (required for all new features)
- All new/modified APIs include changes to the [JavaScript driver](https://github.com/workchat/workchat-redux/blob/master/src/client/client4.js)
-->

#### Summary
<!--
A description of what this pull request does.
-->

#### Ticket Link
<!--
If this pull request addresses a Help Wanted ticket, please link the relevant GitHub issue, e.g.

  Fixes https://github.com/workchat/workchat-server/issues/XXXXX

Otherwise, link the JIRA ticket.
-->
