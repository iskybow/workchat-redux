# Code Contribution Guidelines

Thank you for your interest in contributing! Please see the [Workchat Contribution Guide](https://developers.workchat.com/contribute/getting-started/) which describes the process for making code contributions across Workchat projects and [join our "Contributors" community channel](https://community.workchat.com/core/channels/tickets) to ask questions from community members and the Workchat core team.

When you submit a pull request, it goes through a [code review process outlined here](https://developers.workchat.com/contribute/getting-started/code-review/).
